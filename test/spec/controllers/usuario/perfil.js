'use strict';

describe('Controller: UsuarioPerfilCtrl', function () {

  // load the controller's module
  beforeEach(module('appApp'));

  var UsuarioPerfilCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    UsuarioPerfilCtrl = $controller('UsuarioPerfilCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(UsuarioPerfilCtrl.awesomeThings.length).toBe(3);
  });
});
