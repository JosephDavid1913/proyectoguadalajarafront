'use strict';

describe('Controller: UsuarioRegisterCtrl', function () {

  // load the controller's module
  beforeEach(module('appApp'));

  var UsuarioRegisterCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    UsuarioRegisterCtrl = $controller('UsuarioRegisterCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(UsuarioRegisterCtrl.awesomeThings.length).toBe(3);
  });
});
