'use strict';

describe('Controller: InstructoresListadoCtrl', function () {

  // load the controller's module
  beforeEach(module('appApp'));

  var InstructoresListadoCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    InstructoresListadoCtrl = $controller('InstructoresListadoCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(InstructoresListadoCtrl.awesomeThings.length).toBe(3);
  });
});
