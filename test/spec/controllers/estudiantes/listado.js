'use strict';

describe('Controller: EstudiantesListadoCtrl', function () {

  // load the controller's module
  beforeEach(module('appApp'));

  var EstudiantesListadoCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EstudiantesListadoCtrl = $controller('EstudiantesListadoCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(EstudiantesListadoCtrl.awesomeThings.length).toBe(3);
  });
});
