'use strict';

describe('Controller: EstudiantesFrmestudiantesCtrl', function () {

  // load the controller's module
  beforeEach(module('appApp'));

  var EstudiantesFrmestudiantesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EstudiantesFrmestudiantesCtrl = $controller('EstudiantesFrmestudiantesCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(EstudiantesFrmestudiantesCtrl.awesomeThings.length).toBe(3);
  });
});
