'use strict';

describe('Controller: CoursesListCtrl', function () {

  // load the controller's module
  beforeEach(module('appApp'));

  var CoursesListCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CoursesListCtrl = $controller('CoursesListCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(CoursesListCtrl.awesomeThings.length).toBe(3);
  });
});
