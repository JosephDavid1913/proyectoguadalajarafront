'use strict';

describe('Service: instructors', function () {

  // load the service's module
  beforeEach(module('appApp'));

  // instantiate service
  var instructors;
  beforeEach(inject(function (_instructors_) {
    instructors = _instructors_;
  }));

  it('should do something', function () {
    expect(!!instructors).toBe(true);
  });

});
