'use strict';

describe('Service: interceptorHttp', function () {

  // load the service's module
  beforeEach(module('appApp'));

  // instantiate service
  var interceptorHttp;
  beforeEach(inject(function (_interceptorHttp_) {
    interceptorHttp = _interceptorHttp_;
  }));

  it('should do something', function () {
    expect(!!interceptorHttp).toBe(true);
  });

});
