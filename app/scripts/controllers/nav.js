'use strict';

/**
 * @ngdoc function
 * @name appApp.controller:NavCtrl
 * @description
 * # NavCtrl
 * Controller of the appApp
 */
angular.module('appApp').controller('NavCtrl', function ($scope, $state, $location, store) {

  $scope.stateGo = function(state){
    $state.go(state);
  };

  $scope.salir = function(){
    store.remove('token');
    $state.go('app-login');
  };

});
