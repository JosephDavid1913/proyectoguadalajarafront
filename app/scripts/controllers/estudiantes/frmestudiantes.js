'use strict';

/**
 * @ngdoc function
 * @name appApp.controller:EstudiantesFrmestudiantesCtrl
 * @description
 * # EstudiantesFrmestudiantesCtrl
 * Controller of the appApp
 */
angular.module('appApp').controller('EstudiantesFrmestudiantesCtrl', function ($scope, Students, toaster, Constants, $state) {
    $scope.data = {};
    $scope.defaults = {titulo:'Agregar estudiante'};
    var type = 'save', id = 0;

    if($state.params.id !== null){
        $scope.defaults.titulo = 'Editar estudiante';
        id = $state.params.id;
        type = 'update';
        Students.find({id:id}).$promise.then(function(response){
          $scope.data = response;
        }).catch(function(error){
          console.log(error);
        });
    }

    $scope.save = function(frm){
      if(frm.$valid){
        Students[type]({id:id}, $scope.data).$promise.then(function(reponse){
          toaster.success(Constants.success,"Datos guardado correctamente");
          $state.go("app-estudiantes");
        }).catch(function(error){
          toaster.error(Constants.error,error.data.error.message);
        });
      }
    };

});
