'use strict';

/**
 * @ngdoc function
 * @name appApp.controller:EstudiantesListadoCtrl
 * @description
 * # EstudiantesListadoCtrl
 * Controller of the appApp
 */
angular.module('appApp').controller('EstudiantesListadoCtrl', function ($scope, Students, Constants) {
    $scope.estudiantes = [];
    $scope.currentPage = 1;
    $scope.filter = {limit:Constants.limit,skip:0};

    function searchData(){
      $scope.filter.skip = ($scope.currentPage * $scope.filter.limit) - $scope.filter.limit;
      Students.paginate($scope.filter).$promise.then(function(response){
        $scope.estudiantes = response;
      }).catch(function(error){
        console.log(error);
      });
    }

    searchData();

    Students.count().$promise.then(function(response){
      $scope.totalItems = response.count;
    }).catch(function(error){
      console.log(error);
    });

    $scope.setPage = function (pageNo) {
      $scope.currentPage = pageNo;
    };

    $scope.pageChanged = function() {
      searchData();
    };
});
