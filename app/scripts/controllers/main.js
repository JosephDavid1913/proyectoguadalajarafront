'use strict';

/**
 * @ngdoc function
 * @name appApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the appApp
 */
angular.module('appApp').controller('MainCtrl', function ($scope, Courses, store, $state) {
    
    if(store.get("reload") === true){
      store.set("reload",false);
      $state.go($state.current,{},{reload:true});
    }


    $scope.stats = {AVG_SCORE:80.3, SCORED:4101};
    $scope.doughnutColors=['#8C9EFF','#DDDDDD'];
    $scope.doughnutOptions = 
    {
      responsive:false, 
      responsiveAnimationDuration:500, 
      maintainAspectRatio:true, 
      cutoutPercentage:80, 
      circumference:Math.PI, 
      rotation:(-1 * Math.PI), 
      legend:{display:false} 
    };

    $scope.colors = ['#45b7cd', '#ff6384', '#ff8e72'];

    $scope.labels = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'];
    $scope.data = [
      [65, -59, 80, 81, -56, 55, -40],
      [28, 48, -40, 19, 86, 27, 90]
    ];
    $scope.datasetOverride = [
      {
        label: "Bar chart",
        borderWidth: 1,
        type: 'bar'
      },
      {
        label: "Line chart",
        borderWidth: 3,
        hoverBackgroundColor: "rgba(255,99,132,0.4)",
        hoverBorderColor: "rgba(255,99,132,1)",
        type: 'line'
      }
    ];
});
