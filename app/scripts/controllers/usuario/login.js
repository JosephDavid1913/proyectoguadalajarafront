'use strict';

/**
 * @ngdoc function
 * @name appApp.controller:UsuarioLoginCtrl
 * @description
 * # UsuarioLoginCtrl
 * Controller of the appApp
 */
angular.module('appApp').controller('UsuarioLoginCtrl', function ($scope, toaster, $state, Users, Constants, store) {
    $scope.data = {ttl:50000};
    

    if(store.get("reload") === false){
      $state.go($state.current,{},{reload:true});
    }

    store.set("reload",true);

    $scope.access = function( frm ){
        if( frm.$valid ){
            Users.login($scope.data).$promise.then(function(response){
                toaster.success("Bienvenido");
                store.set('token',response.id);
                $state.go('app-dashboard');                
            }).catch(function(error){
                toaster.error(Constants.error,error.data.error.message);
            });
        }else{
            toaster.error(Constants.error,"Faltan campos por acompletar");
        }
    };

    $scope.register = function(){
        $state.go('app-register');
    };
});
