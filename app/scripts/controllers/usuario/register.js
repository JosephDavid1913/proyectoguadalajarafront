'use strict';

/**
 * @ngdoc function
 * @name appApp.controller:UsuarioRegisterCtrl
 * @description
 * # UsuarioRegisterCtrl
 * Controller of the appApp
 */
angular.module('appApp').controller('UsuarioRegisterCtrl', function ($scope, toaster, Students, Constants, $state, store) {

  $scope.data = {};
  store.set("reload",true);
  
  $scope.access = function( frm ){
    if( frm.$valid ){
      Students.save($scope.data).$promise.then(function(response){
        toaster.success(Constants.success,"Registro correctamente");
        $state.go('app-login');
      }).catch(function(error){
        toaster.error(Constants.error,error.data.error.message);
      });
    }else{
      toaster.error(Constants.error,"Faltan campos por acompletar");
    }
  };

  $scope.back = function(){
    $state.go('app-login');
  };
    
});
