'use strict';

/**
 * @ngdoc function
 * @name appApp.controller:CoursesListCtrl
 * @description
 * # CoursesListCtrl
 * Controller of the appApp
 */
angular.module('appApp').controller('CoursesListCtrl', function ($scope, Courses) {
    $scope.cursos = [];

    Courses.get().$promise.then(function(response){
       $scope.cursos = response;
    }).catch(function(error){
      console.log(error);
    });
});
