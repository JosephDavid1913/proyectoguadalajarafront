'use strict';

/**
 * @ngdoc function
 * @name appApp.controller:InstructoresListadoCtrl
 * @description
 * # InstructoresListadoCtrl
 * Controller of the appApp
 */
angular.module('appApp').controller('InstructoresListadoCtrl', function ($scope, Instructors) {
     $scope.instructores = [];

    Instructors.get().$promise.then(function(response){
       $scope.instructores = response;
    }).catch(function(error){
      console.log(error);
    });
});
