'use strict';

/**
 * @ngdoc overview
 * @name appApp
 * @description
 * # appApp
 *
 * Main module of the application.
 */
angular
  .module('appApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'chart.js',
    'blockUI',
    'angular-storage',
    'ui.router',
    'camera',
    'ui.bootstrap',
    'toaster',
    'ui.router.state.events'
  ])
  .config(function ($routeProvider, $qProvider, blockUIConfig, $httpProvider, $stateProvider, $urlRouterProvider) {    
    $qProvider.errorOnUnhandledRejections(false);   
    $httpProvider.interceptors.push('interceptorHttp');
     // Change the default overlay message
    blockUIConfig.message = 'Cargando...';
    
    // Change the default delay to 100ms before the blocking is visible
    blockUIConfig.delay = 100;
    // , $stateProvider, $urlRouterProvider
    $stateProvider
      .state('app-login',{
        url:'/',
        templateUrl:'views/usuario/login.html',
        controller: 'UsuarioLoginCtrl'
      })
      .state('app-dashboard',{
        url:'/dashboard',
        templateUrl:'views/main.html',
        controller: 'MainCtrl'
      })
      .state('app-courses',{
        url:'/cursos',
        templateUrl:'views/courses/list.html',
        controller: 'CoursesListCtrl'
      })
      .state('app-estudiantes',{
        url:'/estudiantes',
        templateUrl:'views/estudiantes/listado.html',
        controller: 'EstudiantesListadoCtrl'
      })
      .state('app-estudiantes-agregar',{
        url:'/estudiantes/agregar',
        templateUrl:'views/estudiantes/frmestudiantes.html',
        controller: 'EstudiantesFrmestudiantesCtrl'
      })
      .state('app-estudiantes-editar',{
        url:'/estudiantes/:id',
        templateUrl:'views/estudiantes/frmestudiantes.html',
        controller: 'EstudiantesFrmestudiantesCtrl'
      })
      .state('app-instructores',{
        url:'/instructores',
        templateUrl:'views/instructores/listado.html',
        controller: 'InstructoresListadoCtrl'
      })
      .state('app-perfil',{
        url:'/perfil',
        templateUrl:'views/usuario/perfil.html',
        controller: 'UsuarioPerfilCtrl'
      })
      .state('app-register',{
        url:'/registro',
        templateUrl:'views/usuario/register.html',
        controller: 'UsuarioRegisterCtrl'
      });
     $urlRouterProvider.otherwise('/');
  }).run(function ($rootScope, $location, toaster, store) {
      

       $rootScope.$on('$stateChangeStart',function(event, toState, toParams, fromState, fromParams){ 
              var isLogin = true;
              var token = store.get("token") || null;
              if($location.path() === '/' || $location.path() === '/registro' || !token){
                  isLogin = false;              
              }
              setTimeout(function(){ 
                $rootScope.$apply(function(){
                  $rootScope.showLogin = isLogin;
                }); 
              },200);
        });
  });
