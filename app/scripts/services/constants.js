'use strict';

/**
 * @ngdoc service
 * @name appApp.constants
 * @description
 * # constants
 * Constant in the appApp.
 */
angular.module('appApp').constant('Constants', {
    "API":"http://localhost:3000/api/",
    "limit":10,
    "nombre" : "Proyecto",
    "success" : "Éxito",
    "error" : "Ocurrio un error"
  });
