'use strict';

/**
 * @ngdoc service
 * @name appApp.users
 * @description
 * # users
 * Service in the appApp.
 */
angular.module('appApp').service('Users', function ($resource, Constants) {
    return $resource( Constants.API + 'Users/:id', { id:'@id' },{
  		'get': {
  			method:'GET',
  			isArray: true
  		},
  		'login': {
  			url: Constants.API + 'Users/login',
  			method:'POST',
  			isArray: false
  		}
    });
});
