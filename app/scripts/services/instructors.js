'use strict';

/**
 * @ngdoc service
 * @name appApp.instructors
 * @description
 * # instructors
 * Service in the appApp.
 */
angular.module('appApp').service('Instructors', function ($resource, Constants) {
    return $resource( Constants.API + 'instructors/:id', { id:'@id' },{
  		'get': {
  			method:'GET',
  			isArray: true
  		},
  		'save': {
  			url: Constants.API + 'instructors',
  			method:'POST',
  			isArray: false
  		},
  		'update': {
  			method:'PUT',
  			isArray: false
  		},
  		'query': {
  			method:'GET',
  			isArray: false
  		},
  		'delete': {
  			method:'DELETE',
  			isArray: false
  		},
  		'paginado': {
  			url: Constants.API + 'instructors/paginado',
  			method:'GET',
  			isArray: false
  		}
    });
});
