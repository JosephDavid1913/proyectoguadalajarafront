'use strict';

/**
 * @ngdoc service
 * @name appApp.students
 * @description
 * # students
 * Service in the appApp.
 */
angular.module('appApp').service('Students', function ($resource, Constants) {
    return $resource( Constants.API + 'students/:id', { id:'@id' },{
  		'get': {
  			method:'GET',
  			isArray: true
  		},
		'find': {
  			method:'GET',
  			isArray: false
  		},
  		'save': {
  			url: Constants.API + 'students',
  			method:'POST',
  			isArray: false
  		},
  		'update': {
  			method:'PUT',
  			isArray: false
  		},
  		'query': {
  			method:'GET',
  			isArray: false
  		},
  		'delete': {
  			method:'DELETE',
  			isArray: false
  		},
  		'count': {
  			url: Constants.API + 'students/count',
  			method:'GET',
  			isArray: false
  		},
  		'paginate': {
  			url: Constants.API + 'students?filter[limit]=:limit&filter[skip]=:skip',
			params:{ 
				skip:'@skip', 
				limit:'@limit'
			},
  			method:'GET',
  			isArray: true
  		}
    });
});
