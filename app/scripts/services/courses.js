'use strict';

/**
 * @ngdoc service
 * @name appApp.courses
 * @description
 * # courses
 * Service in the appApp.
 */
angular.module('appApp').service('Courses', function ($resource, Constants) {
   return $resource( Constants.API + 'courses/:id', { id:'@id' },{
  		'get': {
  			method:'GET',
  			isArray: true
  		},
  		'save': {
  			url: Constants.API + 'courses',
  			method:'POST',
  			isArray: false
  		},
  		'update': {
  			method:'PUT',
  			isArray: false
  		},
  		'query': {
  			method:'GET',
  			isArray: false
  		},
  		'delete': {
  			method:'DELETE',
  			isArray: false
  		},
  		'paginado': {
  			url: Constants.API + 'courses/paginado',
  			method:'GET',
  			isArray: false
  		}
    });
});
