'use strict';

/**
 * @ngdoc service
 * @name appApp.interceptorHttp
 * @description
 * # interceptorHttp
 * Factory in the appApp.
 */
angular.module('appApp').factory('interceptorHttp', function (store) {
    return {
      request: function (config) {
        if(store.get("token") !==  null)
          config.headers['Authorization'] = store.get("token");
        config.headers['Accept'] = 'application/json';

        return config;
      }
    };
  });
