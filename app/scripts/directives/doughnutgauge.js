'use strict';

/**
 * @ngdoc directive
 * @name appApp.directive:doughnutGauge
 * @description
 * # doughnutGauge
 */
angular.module('appApp')
  .directive('doughnutGauge', function () {
    return {
      restrict: 'E',
     scope: 
      {
          gaugeSize: '@', //size in px
          gaugeOptions: '=',
          gaugeColors: '=',
          gaugeValue: '@', //value displayed 0-100
          valueSuffix: '@', //optional suffix, e.g. '%'
          gaugeTitle: '@', //large title under gauge
          subTitle: '@',  //smaller text under title
      },
      link: function($scope, $element, $attrs) { 
          $attrs.$observe('gaugeValue', function(value) {
            if (value) {
              $scope.gaugeData=[value,100-value]; 
            }
          });
          $scope.gaugeLabels=["Score",'']; 
      },
      template: 
          '<div ng-if="gaugeSize" style="text-align:center;">'+
          '<canvas class="chart chart-doughnut" height="{{gaugeSize}}" width="{{gaugeSize}}" '+
          'chart-legend="true" chart-options="gaugeOptions" chart-colors="gaugeColors" '+
          'chart-data="gaugeData" chart-labels="gaugeLabels">'+
          '</canvas> '+
          '<div style="margin-right:130px; margin-top:{{(-gaugeSize/2.2)}}px; font-size:{{(gaugeSize/6)}}px; font-weight:bold;">{{gaugeValue}}{{valueSuffix}}</div>'+
          '<!--<div style="margin-right:130px; font-size:{{(gaugeSize/7.5)}}px;">{{(gaugeValue)?gaugeTitle:\'No Data\'}}</div>-->'+
          '<!--<div style="margin-right:130px; font-size:{{(gaugeSize/10)}}px;">{{subTitle}}</div>--></div>'
      };
  });
